package hospital;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class HospitalFinder {

	public static void main(String[] args) throws SQLException {
		Scanner sc =new Scanner(System.in);
		
//		Hospital h1 = new Hospital("Apolo",List.of("ENT","Ortho","Gastro"),"Charan","9876598765","Hyderabad");
//        Hospital h2 = new Hospital("Care",List.of("ENT","Ortho","Cardiac"),"Rohit","9856756793","Vizag");
//        Hospital h3 = new Hospital("Seven Hills",List.of("ENT","Gastro"),"Rahul","7895687635","Chennai");
//        Hospital h4 = new Hospital("Q1",List.of("ENT","Ortho","Cardiac","pediatric"),"Kohli","769456234","Banglore");
//        Hospital h5 = new Hospital("Vijaya",List.of("ENT","Ortho"),"Dhoni","9634526798","Kolkata");
//        Hospital h6 = new Hospital("xyz",List.of("ENT","Ortho"),"bala","9634526798","Kolkata");
//        Hospital h7 = new Hospital("abc",List.of("ENT","Ortho"),"mani","9634526798","Kolkata");


        
        HospitalService service = new HospitalService();
        
//        service.addHospital(h1);
//        service.addHospital(h2);
//        service.addHospital(h3);
//        service.addHospital(h4);
//        service.addHospital(h5);
//        service.addHospital(h6);
//         service.addHospital(h7);


        
        HashMap<Integer,String> res = service.getHospitals();
        System.out.println(res);
        
        System.out.println("enter the hospital code to view hospital details");
        service.getHospitalDetails(sc.nextInt());

	}

}
