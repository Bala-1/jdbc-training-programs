package hospital;

import java.sql.*;
import java.util.HashMap;

public class HospitalService {
	/**
	 * 
	 * this method will add hospatal records to db
	 * 
	 **/

	public void addHospital(Hospital obj) throws SQLException {

		String treatments = ""; 
		
		for(String i:obj.getListOfTreatments()) {
			treatments = treatments +" "+ i ;
		}
		
		obj.setHospitalCode(getuniqcode());
		
		Connection myConn = getDBbConnection();

		String query = "insert into hospital values(?,?,?,?,?,?)";
		
		PreparedStatement pst = myConn.prepareStatement(query);

		pst.setInt(1, obj.getHospitalCode());
		pst.setString(2, obj.getHospitalName());
		pst.setString(3, treatments);
		pst.setString(4, obj.getcontactPerson());
		pst.setString(5, obj.getcontactNumber());
		pst.setString(6, obj.getlocation());

		pst.executeUpdate();

		System.out.println("inserted sucessfully");
	}

	private int  getuniqcode() throws SQLException {
		Connection myConn = getDBbConnection();	
		Statement myStat = myConn.createStatement();
		
		ResultSet rs = myStat.executeQuery("SELECT MAX(hospitalcode) as recentcode FROM hospital");
		rs.next();
		return rs.getInt("recentcode")+1;
		
	}

	/**
	 * 
	 * this method returns hospital code and hospital name in form of map variable
	 * 
	 **/

	public HashMap<Integer, String> getHospitals() {

		HashMap<Integer, String> map = new HashMap<>();

		try {
			Connection myConn = getDBbConnection();

			Statement myStat = myConn.createStatement();

			ResultSet rs = myStat.executeQuery("select * from hospital");

			while (rs.next()) {
				map.put(rs.getInt("hospitalCode"), rs.getString("hospitalName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 
	 * this method will take hospital code as argument and print matching hospital details from the database
	 * 
	 * **/
	public void getHospitalDetails(int code) {
		try {
			Connection myConn = getDBbConnection();
			
			PreparedStatement pst = myConn.prepareStatement("select * from hospital where hospitalCode = ?");
			pst.setInt(1, code);

			ResultSet rs = pst.executeQuery();
			
//			rs.next();
//			
//			System.out.println("hospital code : " + rs.getInt("hospitalCode") + 
//			           "," + " hospital name : "+ rs.getString("hospitalName") +
//			           "," + " list of treatments : "+ rs.getString("listOfTreatments") +
//			           "," + " contact person :" + rs.getString("contactPerson") +
//			           "," + " contact Number :" + rs.getString("contactNumber") +
//			           "," + " location :"+ rs.getString("location"));


			while (rs.next()) {
					System.out.println("hospital code : " + rs.getInt("hospitalCode") + 
							           "," + " hospital name : "+ rs.getString("hospitalName") + 
							           "," + " list of treatments : "+ rs.getString("listOfTreatments") +
							           "," + " contact person :" + rs.getString("contactPerson") +
							           "," + " contact Number :" + rs.getString("contactNumber") +
							           "," + " location :"+ rs.getString("location"));
				}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	
	/**
	 * 
	 * method to get db connectivity
	 * 
	 * **/
	Connection getDBbConnection() throws SQLException{
		String url = "jdbc:oracle:thin:@localhost:1521:xe";
		String userName = "system";
		String pwd = "admin@123";
		
		Connection myConn = DriverManager.getConnection(url, userName, pwd);
		return myConn;
	}
}
