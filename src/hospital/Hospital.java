package hospital;

import java.util.List;

public class Hospital {

	  private int hospitalCode;
	    static int codeGen = 1000;
	    private String hospitalName;
	    private List<String> listOfTreatments;
	    private String contactPerson;
	    private String contactNumber;
	    private String location;
	    
	    public Hospital() {
	      //  this.hospitalCode = codeGen++;
	    }



	   public Hospital(String hospitalName, List<String> listOfTreatments, String contactPerson, String contactNumber,
	            String location) {
	        
	        this();
	        this.hospitalName = hospitalName;
	        this.listOfTreatments = listOfTreatments;
	       this.contactPerson = contactPerson;
	       this. contactNumber = contactNumber;
	        this.location = location;
	    }



	   public int getHospitalCode() {
	        return hospitalCode;
	    }



	   public void setHospitalCode(int hospitalCode) {
	        this.hospitalCode = hospitalCode;
	    }



	   public String getHospitalName() {
	        return hospitalName;
	    }



	   public void setHospitalName(String hospitalName) {
	        this.hospitalName = hospitalName;
	    }



	   public List<String> getListOfTreatments() {
	        return listOfTreatments;
	    }



	   public void setListOfTreatments(List<String> listOfTreatments) {
	        this.listOfTreatments = listOfTreatments;
	    }



	   public String getcontactPerson() {
	        return contactPerson;
	    }



	   public void setcontactPerson(String contactPerson) {
	       this.contactPerson = contactPerson;
	    }



	   public String getcontactNumber() {
	        return contactNumber;
	    }



	   public void setcontactNumber(String contactNumber) {
	        this.contactNumber = contactNumber;
	    }



	   public String getlocation() {
	        return location;
	    }



	   public void setlocation(String location) {
	        this.location = location;
	    }



	   @Override
	    public String toString() {
	        return "Hospital [hospitalCode=" + hospitalCode + ", hospitalName=" + hospitalName + ", listOfTreatments="
	                + listOfTreatments + ", contactPerson=" + contactPerson + ", contactNumber=" + contactNumber
	                + ", location=" + location + "]";
	    }
	
}
