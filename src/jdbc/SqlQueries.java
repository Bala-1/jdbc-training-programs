package jdbc;

import java.sql.*;

public class SqlQueries {

	public static void main(String[] args) {

		String url ="jdbc:oracle:thin:@localhost:1521:xe";
		String userName="system";
		String pwd="admin@123";
		
		try {
			Connection myConn = DriverManager.getConnection(url,userName,pwd);
			Statement myStat = myConn.createStatement();
			
			ResultSet rs = myStat.executeQuery("select * from student");
			
			displayData(rs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	public static void displayData(ResultSet rs) {
		// TODO Auto-generated method stub
		try {
			while(rs.next()) {
				System.out.println(rs.getInt(1)+" "+ rs.getString(2)+" "+rs.getString(3));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
