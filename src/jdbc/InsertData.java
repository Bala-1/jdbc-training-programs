package jdbc;

import java.sql.*;

public class InsertData {

	public static void main(String[] args) throws SQLException {

		String url = "jdbc:oracle:thin:@localhost:1521:xe";
		String userName="system";
		String pwd = "admin@123";
		
		Connection myConn = DriverManager.getConnection(url,userName,pwd);
		Statement myStat = myConn.createStatement();
		
//		myStat.executeUpdate("Insert into Student values(1,'bala',98)");
		myStat.executeUpdate("Insert into Student values(2,'bala',97)");
		myStat.executeUpdate("Insert into Student values(3,'mani',92)");
		myStat.executeUpdate("Insert into Student values(4,'kumar',95)");
		myStat.executeUpdate("Insert into Student values(5,'ravi',98)");

		
		System.out.println("data inserted sucessfully");
		
		myConn.close();
	}

}
