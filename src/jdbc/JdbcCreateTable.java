package jdbc;

import java.sql.*;

public class JdbcCreateTable {
	public static void main(String[] args) throws SQLException {
		
		String dbUrl = "jdbc:oracle:thin:@localhost:1521:xe";
		String user = "system";
		String pwd = "admin@123";
		

		
		Connection myConn = DriverManager.getConnection(dbUrl,user,pwd);
		
		Statement myStat = myConn.createStatement();
		
		//creating table in db
		myStat.executeUpdate("create table Student(sno number(4),sname varchar(20),marks number(3))");
		
		System.out.println("table created");		
		myConn.close();
	}

}
