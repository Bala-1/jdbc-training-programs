package jdbc;

import java.sql.*;
import java.util.Scanner;

public class PrepStatement {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		String dbUrl = "jdbc:oracle:thin:@localhost:1521:xe";
		String user = "system";
		String pwd = "admin@123";
				
		Connection myConn = DriverManager.getConnection(dbUrl,user,pwd);
		 
		
		//String query ="insert into student values(?,?,?)"; 
		String  query = "select * from student where marks < ?";

		PreparedStatement pst = myConn.prepareStatement(query);
		
//		Scanner sc= new Scanner(System.in);
//		System.out.println("enter the student number: ");
//		int sno = sc.nextInt();
//		sc.nextLine();
//		System.out.println("enter the student name: ");
//		String sname = sc.nextLine();
//		System.out.println("enter the student marks: ");
//		int marks = sc.nextInt();
		
		//pst.setInt(1,sno);	
		//pst.setString(2,sname);	
		//pst.setInt(3,marks);
		
		
		
		pst.setInt(1, 97);
		ResultSet rs = pst.executeQuery();
		//pst.executeQuery();
		
		//System.out.println("Data inserted sucessfully");	
		
		try {
			while(rs.next()) {
				System.out.println(rs.getInt(1)+" "+ rs.getString(2)+" "+rs.getString(3));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		myConn.close();
		
		//SqlQueries.main(null);

	}

}
